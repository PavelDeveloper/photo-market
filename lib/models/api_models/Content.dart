
import 'dart:async';
import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;

Future<AllContentData> fetchPost(http.Client client) async {
  final response =
  await http.get('http://194.50.254.5:2000/logic/getcontent.php');

  if (response.statusCode == 200) {
    // If the call to the server was successful, parse the JSON
   // return compute(parseContent, response.body);
    return  AllContentData.fromJson(json.decode(response.body));
  } else {
    // If that call was not successful, throw an error.
    throw Exception('Failed to loading');
  }
}

class AllContentData {
  List<Content> contentList;

  AllContentData({this.contentList});

  factory AllContentData.fromJson(Map<String, dynamic> parsedJson){
    var list = parsedJson['content'] as List;
    List<Content> imagesList = list.map((i) => Content.fromJson(i)).toList();
    return AllContentData( contentList: imagesList);
  }
}

class Content {
  final String name;
  final Category category;

  Content({this.name, this.category});

  factory Content.fromJson(Map<String, dynamic> parsedJson){
    var name = parsedJson['name'] as String;
    var category = parsedJson['category'];
    return Content(
        name: name,
        category: Category.fromJson(category),
    );
  }
}

class Category {

   final String type;
   final String size;
   final String sideSize;
   final String photonumb;
   final String price;

   Category({this.type, this.size, this.sideSize, this.photonumb, this.price});

   factory Category.fromJson(Map<String, dynamic> json) {
     return Category(
       type: json['type'] as String,
       size: json['size'] as String,
       sideSize: json['sideSize'] as String,
       photonumb: json['photonumb'] as String,
       price: json['price'] as String,
     );
   }
 }