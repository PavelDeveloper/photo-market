import 'package:flutter/widgets.dart';
import 'package:photo_shop/api/api_client.dart';
import 'package:photo_shop/blocs/content_block.dart';

class ContentProvider extends InheritedWidget {
  final ContentBlock contentBlock;

  @override
  updateShouldNotify(InheritedWidget oldWidget) => true;

  static ContentBlock of(BuildContext context) =>
      (context.inheritFromWidgetOfExactType(ContentProvider) as ContentProvider)
          .contentBlock;

  ContentProvider({Key key, ContentBlock contentBlock, Widget child})
      : this.contentBlock = contentBlock ?? ContentBlock(ApiClient()),
        super(child: child, key: key);
}
