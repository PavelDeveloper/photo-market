import 'dart:async';

import 'package:photo_shop/api/api_client.dart';
import 'package:photo_shop/models/api_models/Content.dart';
import 'package:rxdart/rxdart.dart';

class ContentBlock {
  final ApiClient apiClient;

  Stream<AllContentData> _results = Stream.empty();

  BehaviorSubject<String> _pageName =
      BehaviorSubject<String>(seedValue: 'getcontent.php');

  Stream<AllContentData> get results => _results;
  Sink<String> get pageName => _pageName;

  ContentBlock(this.apiClient) {
    _results = _pageName
        .asyncMap((page) => apiClient.getAllContentData(pageName: page))
        .asBroadcastStream();
  }

void dispose() {
    _pageName.close();
}
}
