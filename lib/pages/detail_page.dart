import 'package:flutter/material.dart';
import 'package:path/path.dart';
import 'package:photo_shop/api/api_client.dart';
import 'package:photo_shop/blocs/content_block.dart';
import 'package:photo_shop/models/api_models/Content.dart';
import 'package:photo_shop/pages/order_page.dart';
import 'package:photo_shop/providers/content_provider.dart';

class DetailPage extends StatelessWidget {
  final String appBarTitle;
  final String source;

  DetailPage(@required this.appBarTitle, @required this.source);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: NestedScrollView(
            headerSliverBuilder:
                (BuildContext context, bool innerBoxIsScrolled) {
              return <Widget>[
                SliverAppBar(
                  expandedHeight: 200.0,
                  floating: false,
                  pinned: true,
                  flexibleSpace: FlexibleSpaceBar(
                      centerTitle: true,
                      title: Text(appBarTitle,
                          style: TextStyle(
                            color: Colors.white,
                            fontFamily: 'Montserrat',
                            fontSize: 16.0,
                          )),
                      background: Hero(
                          tag: appBarTitle,
                          child: Image.asset(source, fit: BoxFit.cover))),
                ),
              ];
            },
            body: ContentProvider(
                contentBlock: ContentBlock(ApiClient()),
                child: RootApp(appBarTitle))));
  }
}

class RootApp extends StatelessWidget {
  String appBarTitle;

  RootApp(this.appBarTitle);

  @override
  Widget build(BuildContext context) {
    final contentBlock = ContentProvider.of(context);
    return ListPage('getcontent.php', contentBlock, appBarTitle);
  }
}

class ListPage extends StatelessWidget {
  final ContentBlock block;
  final String pageName;
  String appBarTitle;

  ListPage(this.pageName, this.block, this.appBarTitle);

  @override
  Widget build(BuildContext context) {
    block.pageName.add(pageName);
    return StreamBuilder(
        stream: block.results,
        builder:
            (BuildContext context, AsyncSnapshot<AllContentData> snapshot) {
          if (!snapshot.hasData)
            return Center(
              child: CircularProgressIndicator(),
            );
          return ListView(
            children: _buildList(snapshot.data.contentList, context),
          );
        });
  }

  List<Widget> _buildList(List<Content> contentList, BuildContext context) {
    List<Content> pageContentList = List<Content>();
    contentList.forEach((Content content) {
      if (content.name == appBarTitle) {
        pageContentList.add(content);
      }
    });
    return pageContentList
        .map((Content f) => Column(
          children: <Widget>[
            ListTile(
                title: Text('${f.category.size} ${f.category.type}'),
                //subtitle: Text(f.category.sideSize),
                trailing: Text('${f.category.price} грн',
                    style: TextStyle(fontFamily: 'Montserrat')),
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => OrderPage(content: f),
                    ),
                  );
                }),
          ],
        ))
        .toList();
  }
}
