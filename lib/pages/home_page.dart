import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:photo_shop/models/Product.dart';
import 'package:photo_shop/pages/basket_page.dart';
import 'package:photo_shop/pages/delivery_page.dart';
import 'package:photo_shop/pages/detail_page.dart';
import 'package:photo_shop/pages/market_page.dart';
import 'package:photo_shop/pages/price_page.dart';
import 'package:photo_shop/pages/support_page.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => new _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<Product> product = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _getProducts();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Фотоцех', style: TextStyle(fontFamily: 'Montserrat')),
          backgroundColor: Colors.deepOrange,
        ),
        drawer: Drawer(
          child: _buildListView(context),
        ),
        body: _myStaggeredGridView());
  }

  ListView _buildListView(BuildContext context) {
    return ListView(
      children: <Widget>[
        UserAccountsDrawerHeader(
          decoration: BoxDecoration(
              image: DecorationImage(
                  fit: BoxFit.cover,
                  image: AssetImage('assets/background.png'))),
        ),
        ListTile(
          title: Text('Купить', style: TextStyle(fontFamily: 'Montserrat')),
          leading: Container(
            child: Image(
                image: AssetImage('assets/shopping.png'),
                width: 32,
                height: 32),
          ),
          onTap: () {
            Navigator.pop(context);
            Navigator.of(context).push(MaterialPageRoute(
                builder: (BuildContext context) => MarketPage()));
          },
        ),
        ListTile(
          title:
              Text('Товары и цены', style: TextStyle(fontFamily: 'Montserrat')),
          leading: Container(
            child: Image(
                image: AssetImage('assets/price.png'), width: 32, height: 32),
          ),
          onTap: () {
            Navigator.pop(context);
            Navigator.of(context).push(MaterialPageRoute(
                builder: (BuildContext context) => PricePage()));
          },
        ),
        ListTile(
          title: Text('Корзина', style: TextStyle(fontFamily: 'Montserrat')),
          leading: Container(
            child: Image(
                image: AssetImage('assets/basket.png'), width: 32, height: 32),
          ),
          onTap: () {
            Navigator.pop(context);
            Navigator.of(context).push(MaterialPageRoute(
                builder: (BuildContext context) => BasketPage()));
          },
        ),
        ListTile(
          title: Text('Доставка', style: TextStyle(fontFamily: 'Montserrat')),
          leading: Container(
            child: Image(
                image: AssetImage('assets/delivery.png'),
                width: 32,
                height: 32),
          ),
          onTap: () {
            Navigator.pop(context);
            Navigator.of(context).push(MaterialPageRoute(
                builder: (BuildContext context) => DeliveryPage()));
          },
        ),
        ListTile(
          title: Text('Поддержка',
              style: TextStyle(fontFamily: 'Montserrat-Bold')),
          leading: Container(
            child: Image(
                image: AssetImage('assets/support.png'), width: 32, height: 32),
          ),
          onTap: () {
            Navigator.pop(context);
            Navigator.of(context).push(MaterialPageRoute(
                builder: (BuildContext context) => SupportPage()));
          },
        ),
      ],
    );
  }

  Widget _myStaggeredGridView() {
    return Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.only(top: 5.0, bottom: 5.0),
        child: SafeArea(
            child: StaggeredGridView.countBuilder(
          scrollDirection: Axis.vertical,
          crossAxisCount: 4,
          itemCount: product.length,
          itemBuilder: (BuildContext context, int index) =>
              _Tile(product[index].imagePath, product[index].name, index),
          staggeredTileBuilder: (int index) =>
              StaggeredTile.count(2, index.isEven ? 2.8 : 2.6),
          mainAxisSpacing: 0,
          crossAxisSpacing: 0,
        )));
  }

  _getProducts() {
    setState(() {
      product.add(Product("Сезонные фотоподарки", "assets/product_01.jpg"));
      product.add(Product("Фотографии", "assets/product_02.jpg"));
      product.add(Product("Полоска из фотобудки", "assets/product_03.jpg"));
      product.add(Product("Фотографии в рамке", "assets/product_04.jpg"));
      product.add(Product("Фотокниги", "assets/product_05.jpg"));
      product.add(Product("Календари", "assets/product_07.jpg"));
      product.add(Product("Печать на холсте", "assets/product_08.jpg"));
      product.add(Product("Печать на пенокартоне", "assets/product_09.jpg"));
      product.add(Product("Мультирамки с фото", "assets/product_010.jpg"));
      product.add(Product("Фотоподушки", "assets/product_011.jpg"));
      product.add(Product("Фотопазлы магнитные", "assets/product_013.jpg"));
      product.add(Product("Фотопазлы картонные", "assets/product_014.jpg"));
      product.add(Product("Магниты", "assets/product_012.jpg"));
      product.add(Product("Кружки", "assets/product_015.jpg"));
      product.add(Product("Чехлы для телефонов", "assets/product_017.jpg"));
      product.add(Product("Коврик для мыши", "assets/product_018.jpg"));
      product.add(Product("Футболки для взрослых", "assets/product_019.jpg"));
      product.add(Product("Футболки для детей", "assets/product_020.jpg"));
    });
  }
}

class _Tile extends StatelessWidget {
  const _Tile(this.source, this.productName, this.index);

  final String source;
  final String productName;
  final int index;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          Navigator.of(context).push(MaterialPageRoute(
              builder: (BuildContext context) =>
                  DetailPage(productName, source)));
        },
        child: _buildCard());
  }

  Card _buildCard() {
    return Card(
      elevation: 2,
      child: Column(
        children: <Widget>[
          Hero(
              tag: productName,
              child: Container(
                height: index.isEven ? 210 : 191,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(5),
                        topRight: Radius.circular(5)),
                    image: DecorationImage(
                        image: AssetImage(source), fit: BoxFit.cover)),
              )),
          Padding(
            padding: const EdgeInsets.all(4.0),
            child: new Column(
              children: <Widget>[
                SizedBox(
                  height: 5.0,
                ),
                Text(productName,
                    textAlign: TextAlign.center,
                    style: TextStyle(fontFamily: 'Montserrat')),
                SizedBox(
                  height: 5.0,
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
