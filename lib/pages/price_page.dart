import 'package:flutter/material.dart';
import 'package:photo_shop/resources/strings_res.dart';

class PricePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return PageState();
  }
}

class PageState extends State<PricePage> {
  List<String> data = [];
  List<String> imagePath = [];
  PageController controller;
  var currentPageValue = 0.0;

  @override
  void initState() {
    super.initState();
    controller = PageController();
    _getProductDescription();
    _getImagesPath();
  }

  @override
  Widget build(BuildContext context) {
    controller.addListener(() {
      setState(() {
        currentPageValue = controller.page;
      });
    });
    return Scaffold(
        appBar: AppBar(
          title:
              Text('Товары и цены', style: TextStyle(fontFamily: 'Montserrat')),
          backgroundColor: Colors.deepOrange,
        ),
        body: PageView.builder(
          controller: controller,
          itemBuilder: (context, position) {
            if (position == currentPageValue.floor()) {
              return _buildTransform(position);
            } else if (position == currentPageValue.floor() + 1) {
              return _buildTransform(position);
            } else {
              return _buildContainer(position);
            }
          },
          itemCount: data.length,
        ));
  }

  Container _buildContainer(int position) {
    return Container(
      color: position % 2 == 0 ? Colors.teal : Colors.cyan,
      child: SingleChildScrollView(
          child: ConstrainedBox(
              constraints: BoxConstraints(),
              child: Column(children: <Widget>[
                SizedBox(
                  height: 15.0,
                ),
                Image.asset(
                  imagePath[position],
                  fit: BoxFit.cover,
                ),
                SizedBox(
                  height: 15.0,
                ),
                Center(
                  child: Text(
                    data[position],
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Colors.white, fontSize: 16.0),
                  ),
                ),
                SizedBox(
                  height: 15.0,
                ),
              ]))),
    );
  }

  Transform _buildTransform(int position) {
    return Transform(
      transform: Matrix4.identity()..rotateX(currentPageValue - position),
      child: _buildContainer(position),
    );
  }

  _getProductDescription() async {
    List<String> des = List<String>();
    des.add(StringRes.seasonPhotoGifts);
    des.add(StringRes.photoBoth);
    des.add(StringRes.photoInFrame);
    des.add(StringRes.photoBook);
    des.add(StringRes.calendar);
    des.add(StringRes.canvas);
    des.add(StringRes.foamCarton);
    des.add(StringRes.multiFrame);
    des.add(StringRes.pillow);
    des.add(StringRes.magnet);
    des.add(StringRes.magnetPuzzle);
    des.add(StringRes.cartonPuzzle);
    des.add(StringRes.cups);
    des.add(StringRes.phoneCover);
    des.add(StringRes.mousePad);
    des.add(StringRes.tShirts);
    data = des;
  }

  _getImagesPath() async {
    List<String> path = List<String>();
    path.add("assets/product_01.jpg");
    path.add("assets/product_03.jpg");
    path.add("assets/product_04.jpg");
    path.add("assets/product_05.jpg");
    path.add("assets/product_07.jpg");
    path.add("assets/product_08.jpg");
    path.add("assets/product_09.jpg");
    path.add("assets/product_010.jpg");
    path.add("assets/product_011.jpg");
    path.add("assets/product_012.jpg");
    path.add("assets/product_013.jpg");
    path.add("assets/product_014.jpg");
    path.add("assets/product_015.jpg");
    path.add("assets/product_017.jpg");
    path.add("assets/product_018.jpg");
    path.add("assets/product_019.jpg");
    imagePath = path;
  }
}
