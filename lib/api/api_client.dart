import 'dart:async';
import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:http/http.dart' show Client;
import 'package:photo_shop/models/api_models/Content.dart';

class ApiClient {

  static const String _url = 'http://194.50.254.5:2000/logic/{0}';

  final Client _client = Client();
  List<String> contentName = List<String>();

  Future<AllContentData> getAllContentData({
    String pageName = "getcontent.php"
  }) async {
    AllContentData contentData;
    await _client.get(Uri.parse(_url.replaceFirst("{0}", pageName)))
        .then((result) => result.body)
        .then(json.decode)
        .then((json) => contentData = AllContentData.fromJson(json));
    return contentData;
  }

}