import 'package:flutter/material.dart';
import 'package:photo_shop/pages/home_page.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Coinmarketcup tacker',
      theme: ThemeData(
          primarySwatch: Colors.deepOrange
      ),
      home: HomePage(),
    );
  }
}